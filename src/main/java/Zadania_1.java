import java.util.List;
import java.util.Scanner;

public class Zadania_1 {
    //wypisuje liczby od 1 do 100
    public static void from1to100() {
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }
    }

    //wypisuje liczby z zakresu określonego przez użytkownika
    public static void printInScope() {
        Scanner scanner = new Scanner(System.in);
        //pobranie początku zakresu
        int start = scanner.nextInt();
        //pobranie końca zakresu
        int stop = scanner.nextInt();
        scanner.close();
        for (int i = start; i <= stop; i++) {
            System.out.println(i);
        }
    }

    //wypisuje liczby z zakresu określonego przez użytkownika
    //kolejne liczby oddalone są o krok określany przez użytkownika
    public static void printInScopeWithStep() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek przedziału:");
        int start = scanner.nextInt();
        System.out.println("Podaj koniec przedziału:");
        int stop = scanner.nextInt();
        System.out.println("Podaj krok:");
        int step = scanner.nextInt();
        scanner.close();
        for (int i = start; i <= stop; i = i + step) {
            System.out.println(i);
        }
    }

    //sprawdzenie czy dana liczba jest parzysta
    public static boolean isOdd(int num) {
        if (num % 2 == 0) {
            return false;
        }

        return true;
    }

    //wyświetlenie znaku dla podanego kodu
    public static void intToChar(int i) {
        char c = (char) i;
        System.out.println("code " + i + " is char: " + c);
    }
    //wyświetlenie kodu dla podanego znaku
    public static void charToInt(char c) {
        int i = (int) c;
        System.out.println("char: " + c + " have code: " + i);
    }

    //sprawdzenie czy kolejna podawana przez użytkownika liczba
    // jest parzysta; brak eleganckiego wyjścia z pętli
    public static void checkIfOdd() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Podaj liczbę:");
            int value = scanner.nextInt();
            boolean result = isOdd(value);
            if (result == true) {
                System.out.println("Is odd");
            } else {
                System.out.println("Is even");
            }
        }
    }

    public static int[] convertWithString(int number) {
        //konwersja liczby na tekst
        String numberString = String.valueOf(number);

        int [] numArr = new int[numberString.length()];

        for (int i = 0; i < numberString.length(); i++) {
            //pobranie znak z przedziału [i, i+1)
            String charStr = numberString.substring(i, i+1);
            //konwersja ciągu znaków (w tym przpadku ciągu 1-no elementowego) na liczbę
            numArr[i] = Integer.valueOf(charStr);
        }
        return numArr;
    }

    //odwrócenie liczby przy użyciu rekurencji
    public static void invertNumber(int num, List<Integer> list) {
        if (num > 0) {
            //wywołanie rekurencyjne
            invertNumber(num/10, list);
            list.add(num%10);
        }
    }

    //obliczanie silni metodą rekurencyjną
    public static int factorial(int a) {
        if (a == 1) {
            return 1;
        }
        //wywołanie rekurencyjne
        return a * factorial(a-1);
    }

    //obliczanie ciągu Fibonacciego
    public static void fib(int max) {
        //wartości dwóch pierwszych wyrazów wynikające z definicji
        int a1 = 1;
        int a2 = 1;
        for (int i = 0; i < max; i++) {
            if (i < 2) {
                //dla dwóch pierwszych po prostu wypisujemy '1'
                System.out.print(1 + " ");
            } else {
                //dla kolejnych obliczamy sumę dwóch poprzednich wyrazów...
                int sum = a2 + a1;
                System.out.print(sum + " ");
                //i aktualizujemy wartości dwóch poprzednich wyrazów
                a2 = a1;
                a1 = sum;
            }
        }
    }

    //obliczanie wyrazu ciągu Fibonacciego metodą rekurencyjną
    public static int fibR(int a) {
        if (a == 0 || a == 1) {
            return 1;
        }
        //wywołanie rekurencyjne
        return fibR(a-2) + fibR(a-1);
    }
}
