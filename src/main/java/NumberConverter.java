public class NumberConverter {
    private int value;

    public NumberConverter(int value) {
        this.value = value;
    }

    public String toBinary() {
        int tmp = value;
        StringBuilder builder = new StringBuilder();

        // 101 = 3
        // 101 << 1  --> 010
        // 101 >> 1  --> 001 / 101
        // 101 >>> 1 --> 001
        // << >> >>>
        // 000000000001
        // 011101001001 &
        // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f
        //jeśli tmp == 0 to wykonaliśmy wszystkie przesunięcia
        while (tmp != 0) {
            //bitowy operator AND - chcemy sprawdzić czy maksymalnie prawy bit w naszej liczbie
            //jest jedynką czy zerem - dlatego 'tmp' & 0x1 (gdzie 0x1: 00000000000000000...01)
            int result = tmp & 0x1;
            //wstawiamy '1' albo '0' na pierwszą pozycję w buforze
            builder.insert(0, result);
            //bitowe przesunięcie liczby 'tmp' o jedną pozycję w prawo (uzupełnianie z lewej - 0)
            tmp = tmp >>> 1;
        }

        return builder.toString();
    }

    public String toHex() {
        int tmp = value;
        StringBuilder builder = new StringBuilder();
        while (tmp != 0) {
            int result = tmp & 0xf;
            builder.insert(0,
                    result <= 9 ? String.valueOf(result) : String.valueOf((char)('a' + result - 10)));
            tmp = tmp >>> 4;
        }

        return builder.toString();
    }
}
