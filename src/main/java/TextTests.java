public class TextTests {
    public static boolean isPalindrome(String input) {
        if (input == null) {
            return true;
        }
        String data = input.toLowerCase();
        int length = data.length();
        for(int i = 0; i < length/2; i++) {
            if (data.charAt(i) != data.charAt(length - i - 1)) {
                return false;
            }
        }

        return true;
    }

    public static boolean isAnagram(String str1, String str2) {
        //jeśli liczba znaków jest różna to nie jest to anagram
        if (str1.length() != str2.length()) {
            return false;
        }
        //dla ułatwienia zamieniamy wszystkie litery na małe
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();

        //tworzymy sobie tablicę liczników dla znaków
        int[] counters = new int['z' - 'a' + 1];
        //dodajemy wystąpienia liter w pierwszym słowie
        for (int i = 0; i < str1.length(); i++) {
            counters[str1.charAt(i) - 'a']++;
        }
        //odejmujemy wystąpienia liter w drugim słowie
        for (int i = 0; i < str2.length(); i++) {
            counters[str2.charAt(i) - 'a']--;
        }
        //sprawdzamy czy każdy znak wystąpił tyle samo razy - jeśli tablica same zera, to znaczy
        //że dla każdego znaku wykonano tyle samo inkrementacji co dekrementacji dla każego ze znaków -
        //czyli każdy znak wystąpił taką samą liczbę razy
        for (int i = 0; i < counters.length; i++) {
            if (counters[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
