package notepad;

import java.util.LinkedList;

public class Notepad {
    //stała definiująca długość nagłówka na liście notatek
    private static final int HEADER_LEN = 10;
    //lista notatek
    private LinkedList<String> notesList = new LinkedList<>();

    //dodawanie notatki
    public void addNote(String note) {
        //sprawdzenie czy notatka nie jest nullem - nie dodajemy nulli
        if (null == note) {
            System.out.println("Ooops, this note is null!");
            return;
        }
        //dodanie notatki do listy (LinkedList)
        notesList.add(note);
    }

    //wydrukowanie notatki
    public void printNote(int index) {
        //sprawdzenie czy index jest poprawny
        //tożsame z następnym if-em
//        if (isValidIndex(index) != true) {
//            return;
//        }
        if (!isValidIndex(index-1)) {
            return;
        }

        //pobranie notatki
        String note = notesList.get((index - 1));
        //wypisanie notatki
        System.out.println("Note " + index + ":" + note);
    }

    //wypisanie listy notatek
    public void list() {
        int counter = 1;
        //inny zapis pętli for (for each) wykonującej się zawsze dla wszystkich elementów
        for (String note : notesList) {
            //obliczenie długości substringa z notatki
            int headerLength = note.length() >= HEADER_LEN ? HEADER_LEN : note.length();
            //pobranie substringa o długości <= 10
            String header = note.substring(0, headerLength);
            StringBuilder builder = new StringBuilder();
            //dodanie numerku notatki do builder
            builder.append(counter + ". ");
            //dodanie skróconego tekstu notatki do builder
            builder.append(header);
            //jeśli oryginalna notatka jest > 10 dodaj "..." na końcu skrótu
            if (note.length() > HEADER_LEN) {
                builder.append("...");
            }
            //wydrukowanie notatki
            System.out.println(builder.toString());
            counter++;
        }
    }

    public void delete(int index) {
        //sprawdzenie poprawności indeksu
        if (!isValidIndex((index - 1))) {
            return;
        }
        //usunięcie notatki o podanym indeksie
        notesList.remove(index);
    }

    //wyczyść wszystkie notatki
    public void clearNotes() {
        //TODO: zaimplementuj mnie
    }

    //zwróć liczbę notatek
    public int getNotesCount() {
        //TODO: zaimplementuj mnie
        return 0;
    }

    //sprawdzenie poprawności indeksu (powinien być w zakresie [0, length - 1]
    private boolean isValidIndex(int index) {
        if (index < 0 || index >= notesList.size()) {
            return false;
        }
        return true;
    }
}
