import java.util.Random;

public class Util {
    //Generuje tablicę liczb pseudolosowych.
    // size - rozmiar tablicy
    // scope - ograniczenie zakresu; dla wartości 0 pełny zakres
    public static int[] getRandomArray(int size, int scope) {
        Random random = new Random();
        int[] tab = new int[size];
        for (int i = 0; i < size; i++) {
            if (scope == 0) {
                tab[i] = random.nextInt();
            } else {
                tab[i] = random.nextInt() % scope;
            }
        }
        return tab;
    }

}