import java.util.Arrays;

public class ArraysOps {
    //wyszukiwanie najwyższe i najniższej wartości w tablicy
    public static void findHighestAndLowest(int[] tab) {
        int min = tab[0];
        int max = tab[0];
        for(int i = 0; i < tab.length; i++) {
            if (min > tab[i]) {
                min = tab[i];
            }
            if (max < tab[i]) {
                max = tab[i];
            }
        }

        System.out.println("Min: " + min);
        System.out.println("Max: " + max);
    }

    //odwracanie tablicy "w miejscu" (bez użycia tablicy pomocniczej)
    public static void reverseArray(int[] arr) {
        System.out.println(Arrays.toString(arr));
        //przechodzimy tylko do połowy tablicy i za pomocą odpowiednich obliczeń na indeksie
        //zamieniamy po kolei (parami) pierwszy z ostatnim, drugi z przedostatnim itd.
        for (int i = 0; i < arr.length/2; i++) {
            int tmp = arr[i];
            arr[i] = arr[arr.length - 1];
            arr[arr.length - 1] = tmp;
        }
        System.out.println(Arrays.toString(arr));
    }

    //obliczanie najczęściej występującego elementu tablicy przy użyciu tablicy liczników
    //założenie: max = 1000000
    public static int findMostFrequent_v2(int[] tab, int max) {
        //tworzymy tablicę liczników dla wartości od 0 do max
        int[] counters = new int[max];
        //przechodzimy przez tablicę 'tab' i zwiększamy liczniki dla każdej wartości
        for (int i = 0; i < tab.length; i++) {
            counters[tab[i]]++;
        }
        //wyszukujemy element o najwyższej wartości czyli ten najczęściej występujący
        int maxFound = counters[0];
        for (int i = 0; i < counters.length; i++) {
            if (max < counters[i]) {
                maxFound = counters[i];
            }
        }
        return maxFound;
    }

    //obliczanie najczęściej występującego elementu tablicy
    public static int findMostFrequent(int[] tab) {
        //ustawiamy liczniki
        int count = 0;
        int maxCount = 0;
        //indeks liczby najczesciej pojawiajacej sie
        int id = 0;
        //dla kazdej liczby w tablicy...
        for (int i = 0; i < tab.length; i++) {
            //porownaj z pozostalymi liczbami...
            for (int j = 0; j < tab.length; j++) {
                //i zwieksz licznik jesli trzeba
                if (tab[i] == tab[j]) {
                    count++;
                }
            }
            //jesli znaleziono nowy max, zaktualizuj wartosc i zapamietaj nowy indeks
            if (maxCount < count) {
                maxCount = count;
                id = i;
            }
            //wyczysc licznik tymczasowy
            count = 0;
        }
        return tab[id];
    }

    //sklejenie dwóch tablic
    public static int[] concatArrays(int[] arr1, int[] arr2) {
        //sprawdzamy czy tablice nie są nullami
        if (arr1 == null) {
            return arr2;
        }
        if (arr2 == null) {
            return arr1;
        }
        //tworzymy tablicę wynikową o długości równej sumie długości tablic składowych
        int[] result = new int[arr1.length + arr2.length];
        //kopiujemy elementy z pierwszej tablicy
        for (int i = 0; i < arr1.length; i++) {
            result[i] = arr1[i];
        }
        //kopiujemy elementy z drugiej tablicy - należy pamiętać o odpowiednim przesunięciu indeksu do
        //tablicy result
        for (int i = 0; i < arr2.length; i++) {
            result[i + arr1.length] = arr2[i];
        }
        return result;
    }

    //sklejenie dwóch posortowanych tablic w jedną, która też jest posortowana
    public static int[] concatArraysSorted(int[] arr1, int[] arr2) {
        //sprawdzamy czy tablice nie są nullami
        if (arr1 == null) {
            return arr2;
        }
        if (arr2 == null) {
            return arr1;
        }
        //tworzymy tablicę wynikową o długości równej sumie długości tablic składowych
        int[] result = new int[arr1.length + arr2.length];
        //tworzymy osobne indeksy dla każdej tablicy składowej
        int a1Idx = 0;
        int a2Idx = 0;
        for (int i = 0; i < result.length; i++) {
            //jeżeli oba indeksy są poprawne weź mniejszy element z odpowiedniej tablicy
            if (a1Idx < arr1.length && a2Idx < arr2.length) {
                if (arr1[a1Idx] < arr2[a2Idx]) {
                    result[i] = arr1[a1Idx];
                    a1Idx++;
                } else {
                    result[i] = arr2[a2Idx];
                    a2Idx++;
                }
            }
            //jeśli indeks do tablicy arr1 przekroczył jej zakres, skopiuj element z tablicy arr2
            else if (a1Idx >= arr1.length) {
                result[i] = arr2[a2Idx];
                a2Idx++;
            }
            //jeśli indeks do tablicy arr2 przekroczył jej zakres, skopiuj element z tablicy arr1
            else {
                result[i] = arr1[a1Idx];
                a1Idx++;
            }
        }
        return result;
    }

    //konwersja liczby całkowitej na tablicę - każda pozycja (jedności, dziesiątki, setki, itd.)
    //zajmuje odpowiednią pozycję w tablicy
    public static int[] convertNumToArray(int number) {
        int digits = 0;
        int tmp = number;
        //obliczenie liczby pozycji w celu określenia długości tablicy
        do {
            digits++;
            tmp /= 10;
        } while (tmp > 0);

        int[] result = new int[digits];
        int exponent = digits - 1;
        int i = 0;
        while(exponent >= 0) {
            //obliczenie wartości na danej pozycji, np. 1124 / 1000 -> 1
            result[i] = number / (int) Math.pow(10, exponent);
            //zmniejszenie konwertowanej liczby, np. 1124 - 1000 -> 124
            number -= result[i] * (int) Math.pow(10, exponent);
            //zmniejszenie wykładnika potęgi
            exponent--;
            i++;
        }
        return result;
    }

    //dodawanie dwóch liczb w systemie dziesiętnym zapisanych w formie tablic
    public static int[] add(int[] arr1, int[] arr2) {
        //sprawdzamy czy tablice nie są nullami
        if (arr1 == null) {
            return arr2;
        }
        if (arr2 == null) {
            return arr1;
        }

        // ternary operator
        //    a > b?      a         :         b
        // if (a > b) { return a; } else { return b; }
        int[] result = new int[arr1.length > arr2.length ? arr1.length : arr2.length];
        //ustawienie indeksów do tablic arr1 i arr2
        int arr1Index = arr1.length - 1;
        int arr2Index = arr2.length - 1;
        //[ 1, 2, 3]
        //    [5, 4]

        //zmienna do zapisywania w pamięci - kiedy suma na pozycji nie mieści się w zakresie 0-9
        int mem = 0;
        //[ 1, 7, 7]
        //wykonujemy działania od prawej strony
        for (int i = result.length - 1; i >= 0; i--) {
            int tmp = 0;
            //oba indeksy są poprawne - sumujemy liczby na danych pozycjach
            if (arr1Index >= 0 && arr2Index >= 0) {
                tmp = arr1[arr1Index] + arr2[arr2Index];
            }
            //indeks arr1Index jest poza zakresem więc tylko przepisujemy z tablicy arr2
            else if (arr1Index < 0) {
                tmp = arr2[arr2Index];
            }
            //indeks arr2Index jest poza zakresem więc tylko przepisujemy z tablicy arr1
            else {
                //arr2Index < 0 && arr1Index >=0
                tmp = arr1[arr1Index];
            }
            //jeśli mamy w pamięci coś z poprzedniej iteracji to dodajemy i czyścimy pamięć
            if (mem > 0) {
                tmp += mem;
                mem = 0;
            }
            //jeśli wynik nie da się zapisać za pomocą jednej cyfry, dodajemy 1 do pamięci
            //i obliczamy resztę
            if (tmp > 9) {
                mem = 1;
                tmp = tmp % 10;
            }
            //wstawiamy wynik do tablicy wynikowej
            result[i] = tmp;
            //przesuwamy indeksy o 1 pozycję w lewo
            arr1Index--;
            arr2Index--;
        }

        //po zakończeniu całej operacji ciągle mamy coś w pamięci
        if (mem > 0) {
            //alokujemy nową tablicę żeby zmieścić dodatkową pozycję
            int[] tmpArr = new int[result.length + 1];
            //na początek nowej tablicy wstawiamy wartość z pamięci
            tmpArr[0] = mem;
            //kopiujemy wszystkie wcześniej obliczone wartości do nowej tablicy
            for (int i = 0; i < result.length; i++) {
                tmpArr[i+1] = result[i];
            }
            //ustawiamy nową tablicę jako result
            result = tmpArr;
        }
        return result;
    }
}
