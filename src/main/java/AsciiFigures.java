public class AsciiFigures {
    public static void drawSquare(int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i < j) {
                    System.out.print("o ");
                } else {
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
    }

    public static void drawDiamond(int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i > j) {
                    if (j >= size - i) {
                        System.out.print("| ");
                    } else {
                        System.out.print("- ");
                    }
                } else {
                    if (i >= size - j) {
                        System.out.print("* ");
                    } else {
                        System.out.print("# ");
                    }
                }
            }
            System.out.println();
        }
    }
}
