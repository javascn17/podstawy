
public class CaesarCrypto {
    private static final int NUM_CHARS = 'z' - 'a';

    private int offset;

    public CaesarCrypto(int offset) {
        this.offset = offset & NUM_CHARS;
    }

    //szyfrowanie wiadomości
    public String cipher(String input) {
        //zamiana wysokości wszystkich liter na małe
        String data = input.toLowerCase();
        //stworzenie klasy buildera do tworzenia String-a po kawałku
        StringBuilder builder = new StringBuilder();
        //przeejście po każdym znaku wiadomości którą szyfrujemy
        for (int i = 0; i < data.length(); i++) {
            char character = data.charAt(i);

            if (character >= 'a' && character <= 'z') {
                //przesunięcie znaku
                character += offset;
                //sprawdzenie czy nie nastąpiło przekroczenie zakresu (a - z)
                if (character > 'z') {
                    //"zawinięcie" znaku
                    character = (char)((character - 'z') + 'a');
                }
            }
            builder.append(character);
        }
        return builder.toString();
    }

    public String decipher(String input) {
        String data = input.toLowerCase();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.length(); i++) {
            char character = data.charAt(i);
            if (character >= 'a' && character <= 'z') {
                //przesunięcie znaku
                character -= offset;
                //sprawdzenie czy nie nastąpiło przekroczenie zakresu (a - z)
                if (character < 'a') {
                    //"zawinięcie" znaku
                    character = (char)((character + 'z') - 'a');
                }
            }
            builder.append(character);
        }
        return builder.toString();
    }
}
